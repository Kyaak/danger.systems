---
title: Getting Started with Danger JS
subtitle: Getting Started with Danger JS
layout: guide_js
order: -1
blurb: Step one on using Danger in your application. Start here.
edit_url: https://gitlab.com/danger-systems/danger.systems/blob/master/static/source/js/guides/getting_started.html.slim
---

ruby:
  classes = JSON.parse(File.read("json_data/js_ci_sources.json"))
  cis = classes.select { |c| c["comment"] && c["comment"]["text"] }
  for source in cis
    # 1st line
    base = source["comment"]["shortText"].strip || ""
    # everything after that
    base = base + "\n\n" + source["comment"]["text"].lines.map{ |l| l.strip }.join("\n")
    # Remove the Swift markers
    base = base.gsub("<!-- JS --!>", "").gsub("<!-- !JS --!>", "")

    # Remove any references to orthere languages
    if base.include? "<!-- Swift --!>"
      base = base.split("<!-- Swift --!>")[0] + base.split("<!-- !Swift --!>")[1]
    end

    base = base.gsub("[run_command]", "yarn danger ci")
    base = base.gsub("[run_command_split]", '["yarn", "danger", "ci"]')

    source["md"] = base
  end


css:

  .seventy div h3:first-child {
    margin-top: 0;
  }

markdown:
  So, you're ready to get set up? You have two options, go through this tutorial or run `yarn add danger --dev; yarn danger init` inside your project file then come back for the CI integration once you're done. Or just get started below.

  There are 5 steps involved in getting Danger running:

  -   [Include the Danger module](#including-danger).
  -   [Creating a Dangerfile](#creating-a-dangerfile) and add a few simple rules.
  -   [Creating an account for Danger to use](#creating-a-bot-account-for-danger-to-use).
  -   Setting up [an access token for Danger](#setting-up-an-access-token) with that account.
  -   Setting up [Danger to run on your CI](#setting-up-danger-to-run-on-your-ci).

  ### Including Danger

  We recommend you install Danger via [Yarn][]. Though you can use the npm CLI.

  ### Installation

  Adding the Danger module by `yarn add danger --dev`. This will add Danger as a `devDependency` and make the command `danger` available by running `yarn danger`.

  ### Creating a Dangerfile

  Create either a new file, either `dangerfile.js` or `dangerfile.ts` will be picked up automatically. Otherwise you can use an argument to pass in a custom file.

  To get yourself started, try this as your Dangerfile:

  ```js
  import {message, danger} from "danger"

  const modifiedMD = danger.git.modified_files.join("- ")
  message("Changed Files in this PR: \n - " + modifiedMD)
  ```

  It will output the list of modified files for your current PR once you have your authentication set up below.

  For a deeper understanding of using a Dangerfile, see the guide [The Dangerfile][dangerfile]

  ### Creating a bot account for Danger to use

  This is optional. Pragmatically, you want to do this though. Currently Danger JS only supports communication on GitHub and BitBucket Server. If you're interested in GitLab support, PRs are welcome, or look at the Ruby version of Danger.

  ### GitHub

  In order to get the most out of Danger, we recommend giving it the ability to post comments in your Pull Requests. This is a regular GitHub account, but depending on whether you are working on a private or public project, you will want to give different levels of access to this bot. You are allowed to have [one bot per GitHub account][github_bots].

  To get started, open [https://github.com](https://github.com) in a private browser session.

  #### OSS Projects

  Do not add the bot to your repo or to your organization.

  #### Closed Source Projects

  Add the bot to your repo or to your organization. The bot requires permission level "Write" to be able to set a PR's status. Note that you _should not_ re-use this bot for OSS projects.

  #### Setting up an Access Token

  [Here's the link][github_token], you should open this in the private session where you just created the new GitHub account. Again, the rights that you give to the token depend on the openness of your projects. You'll want to save this token for later, when you add a `DANGER_GITHUB_API_TOKEN` to your CI.

  #### Tokens for OSS Projects

  We recommend giving the token the smallest scope possible. This means just `public_repo`, this scopes limits Danger's abilities to just writing comments on OSS projects. Because the token can be quite easily be extracted from the CI environment, this minimizes the chance for bad actors to cause chaos with it.

  #### Tokens for Closed Source Projects

  We recommend giving access to the whole `repo` scope, and its children.

  #### Enterprise GitHub

  You can work with GitHub Enterprise by setting 2 environment variables:

  -   `DANGER_GITHUB_HOST` to the host that GitHub is running on.
  -   `DANGER_GITHUB_API_BASE_URL` to the host that the GitHub Enterprise API is reachable on.

  For example:

  ```sh
  DANGER_GITHUB_HOST=git.corp.evilcorp.com
  DANGER_GITHUB_API_BASE_URL=https://git.corp.evilcorp.com/api/v3
  ```

  ### BitBucket Server

  To use Danger JS with BitBucket Server, you'll need to create a new account for Danger to use, then set the following environment variables on your CI:

  * `DANGER_BITBUCKETSERVER_HOST` = The root URL for your server, e.g. `https://bitbucket.mycompany.com`.
  * `DANGER_BITBUCKETSERVER_USERNAME` = The username for the account used to comment.
  * `DANGER_BITBUCKETSERVER_PASSWORD` = The password for the account used to comment.

  This account is then used to provide feedback on your PRs.

  ### Continuous Integration

  Continuous Integration is the process of regularly running tests and generating metrics for a project. It is where you can ensure that the code you are submitting for review is passing on all of the tests. You commonly see this as green or red dots next to commits.

  Danger is built to run as a part of this process, so you will need to have this set up as a pre-requisite.

  ### Setting up Danger to run on your CI

  [jest]: https://facebook.github.io/jest/
  [jest-config]: https://facebook.github.io/jest/docs/configuration.html
  [github_bots]: https://twitter.com/sebastiangrail/status/750844399563608065
  [github_token]: https://github.com/settings/tokens/new
  [yarn]: https://yarnpkg.com
  [dangerfile]: /js/guides/the_dangerfile.html

section.examples
  article
    .split
      .thirty
        ul style="margin-top: 0px;"
          - for source in cis
            - source_id = source["name"].downcase.gsub(" ", "_")
            li class=source_id data-ref="docs_#{source_id}"
              a == source["name"]

      .seventy style="padding:0 20px;"
        - for source in cis
          - source_id = "docs_" + source["name"].downcase.gsub(" ", "_")
          - trimmed = source["comment"]["text"].lines.map{ |l| l.strip }.join("\n")

          div class=source_id style="display:none;"
            == markdown_h(source["md"])

markdown:
  ### Verify Installation

  You should be able to verify that you have successfully integrated Danger by either re-building your CI or pushing your new commits.

  ## What now?

  There are a few of places you can go from here. We'd recommending opening tabs on all these articles:

  -   [The Dangerfile][dangerfile].
  -   [Cultural Changes of Danger][culture].
  -   [The Dangerfile API reference][api].

  Then depending on the type of project you are working on, checking out either [Danger + Node App][app], or [Danger + Node Library][lib]. These should give you good overview of what is possible from here, then it's really up to you to find how you can codify some aspects of your team's culture.

  Good luck!

  [api]: /js/reference.html
  [dangerfile]: /js/guides/the_dangerfile.html
  [culture]: /js/usage/culture.html
  [app]: /js/tutorials/node-app.html
  [lib]: /js/tutorials/node-library.html

== javascript_include_tag  "https://code.jquery.com/jquery-3.1.0.min.js"

javascript:
  /// Generic-ish Example Highlight Code
  $("section.examples ul li").mouseover(function(event) {
    event.preventDefault()
    
    // Set the corresponding code
    var klass = $(this).attr("class").replace(" highlighted", "")

    // Set selection
    $(this).parent().children().removeClass("highlighted")
    $(this).addClass("highlighted")

    var selector = $(this).data("ref")
    $(".seventy div").css("display", "none")

    var md = $("." + selector)
    md.css("display", "block")
  })

  $("section.examples ul li:first-child").trigger('mouseenter')
