---
title: Get Started with Danger
subtitle: Get Started with Danger
layout: guide_sw
order: -1
blurb: Step one on using Danger with Swift in your application. Start here.
edit_url: https://gitlab.com/danger-systems/danger.systems/blob/master/static/source/swift/guides/getting_started.html.slim
---

ruby:
  classes = JSON.parse(File.read("json_data/js_ci_sources.json"))
  cis = classes.select { |c| c["comment"] && c["comment"]["text"] }
  for source in cis
    # 1st line
    base = source["comment"]["shortText"].strip || ""
    # everything after that
    base = base + "\n\n" + source["comment"]["text"].lines.map{ |l| l.strip }.join("\n")
    # Remove the Swift markers
    base = base.gsub("<!-- Swift --!>", "").gsub("<!-- !Swift --!>", "")

    # Remove any references to orthere languages
    if base.include? "<!-- JS --!>"
      base = base.split("<!-- JS --!>")[0] + base.split("<!-- !JS --!>")[1]
    end

    base = base.gsub("[run_command]", "[swift run] danger-swift ci")
    base = base.gsub("[run_command_split]", '["swift", "run", "danger-swift", "ci"]')

    source["md"] = base
  end

css:

  .seventy div h3:first-child {
    margin-top: 0;
  }

markdown:
  So, you're ready to get set up?

  There are 5 steps involved in getting Danger running:

  -   [Installing Danger Locally](#including-danger).
  -   [Creating a Dangerfile](#creating-a-dangerfile) and add a few simple rules.
  -   [Creating an account for Danger to use](#creating-a-bot-account-for-danger-to-use).
  -   Setting up [an access token for Danger](#setting-up-an-access-token) with that account.
  -   Setting up [Danger to run on your CI](#setting-up-danger-to-run-on-your-ci).

  ### Including Danger

  We recommend you install Danger via Swift Package Manager. Though you can use homebrew also. Using Swift PM means 
  you safely lock your versions, making your tooling more reliable.

  ### Installation

  #### Swift PM

  You'll need to be using Xcode 10 or above. We need to create a [package definition][swiftpm_docs], so create a `Package.swift` 
  in the root of your folder:

  ```swift
  // swift-tools-version:4.2
  import PackageDescription

  let package = Package(
      name: "Eigen",
      dependencies: [
        .package(url: "https://github.com/danger/swift.git", from: "0.8.0")
      ],
      targets: [
          // This is just an arbitrary Swift file in our app, that has
          // no dependencies outside of Foundation, the dependencies section
          // ensures that the library for Danger gets build also.
          .target(name: "eigen", dependencies: ["Danger"], path: "Artsy", sources: ["Stringify.swift"]),
      ]
  )
  ```

  This is working around the system a little bit here, we create a SwiftPM build target that requires Danger (to ensure 
  it gets compiled correctly) by adding Danger as a dependency. Next we create a build target that references a random
  Swift file in your app. This is so that libDanger will be created and because you have to have at least one build
  target in a Swift Package. Two birds, one bath.

  You'll have to find a file like this in your codebase, something that just imports `Foundation` is enough.

  Run `swift build` to verify your setup.

  From this point on you can use `swift run danger-swift [cmd]` to run Danger locked correctly to your project.

  #### Homebrew

  This is good for quick experiments, and is macOS only. Install [homebrew][], then run `brew install danger/tap/danger-swift`. 

  Now you have a globally installed version of danger that's available via `danger-swift [cmd]` on your computer.

  This won't be version locked, so you'll always be using the latest version of Danger Swift (and Danger JS) which is kinda
  risky. We're good with testing builds etc, but SemVer breaks in either project will probably occur and you won't have a choice 
  but to migrate at the same time or face failing builds.

  ### Creating a Dangerfile

  Run `[swift run] danger edit` - this will create a default `Dangerfile.swift` and open an Xcode project

  To get yourself started, add this to the `Dangerfile.swift` in the Xcode project:

  ```swift
  let editedFiles = danger.git.modifiedFiles + danger.git.createdFiles
  message("These files have changed: \(editedFiles.joined())")
  ```

  It will output the list of modified files for a PR, which we'll test next. For a deeper understanding of how to 
  write a Dangerfile, see the guide [The Dangerfile][dangerfile].

  ### Testing locally

  Let's try run your new `Dangerfile.swift` against an existing PR. If you're interested in why, see [this doc][arch]. To do 
  that you'll need a copy of Danger JS available. First off, do you have node set up? Test by 
  running `node -v` if you're unsure. If you do, run `npm install -g danger@^6.0.0` to install the latest stable 6.x of Danger.

  If you don't, you can install Danger JS via homebrew: `brew install danger/tap/danger-js`. 

  You can now test your current Dangerfile against an existing PR by running: `[swift run] danger-swift pr [url_of_pr]`. It will only 
  leave messages in your terminal, not on the PR. This will run un-authenticated so it won't work with private repos, 
  if you need a public PR to test, try: `[swift run] danger pr https://github.com/danger/swift/pull/146`.

  ### Creating a bot account for Danger to use

  This is optional. Pragmatically, you want to do this though.

  ### GitHub

  In order to get the most out of Danger, we recommend giving it the ability to post comments in your Pull Requests. This is a regular GitHub account, but depending on whether you are working on a private or public project, you will want to give different levels of access to this bot. You are allowed to have [one bot per GitHub account][github_bots].

  To get started, open [https://github.com](https://github.com) in a private browser session.

  #### OSS Projects

  Do not add the bot to your repo or to your organization.

  #### Closed Source Projects

  Add the bot to your repo or to your organization. The bot requires permission level "Write" to be able to set a PR's
  status. Note that you _should not_ re-use this bot for OSS projects.

  #### Setting up an Access Token

  [Here's the link][github_token], you should open this in the private session where you just created the new 
  GitHub account. Again, the rights that you give to the token depend on the openness of your projects. You'll want to 
  save this token for later, when you add a `DANGER_GITHUB_API_TOKEN` to your CI.

  #### Tokens for OSS Projects

  We recommend giving the token the smallest scope possible. This means just `public_repo`, this scopes 
  limits Danger's abilities to just writing comments on OSS projects. Because the token can be quite easily be 
  extracted from the CI environment, this minimizes the chance for bad actors to cause chaos with it.

  #### Tokens for Closed Source Projects

  We recommend giving access to the whole `repo` scope, and its children.

  #### Enterprise GitHub

  You can work with GitHub Enterprise by setting 2 environment variables:

  -   `DANGER_GITHUB_HOST` to the host that GitHub is running on.
  -   `DANGER_GITHUB_API_BASE_URL` to the host that the GitHub Enterprise API is reachable on.

  For example:

  ```sh
  DANGER_GITHUB_HOST=git.corp.evilcorp.com
  DANGER_GITHUB_API_BASE_URL=https://git.corp.evilcorp.com/api/v3
  ```

  ### BitBucket Server

  To use Danger Swift with BitBucket Server, you'll need to create a new account for Danger to use, then set the following environment variables on your CI:

  * `DANGER_BITBUCKETSERVER_HOST` = The root URL for your server, e.g. `https://bitbucket.mycompany.com`.
  * `DANGER_BITBUCKETSERVER_USERNAME` = The username for the account used to comment.
  * `DANGER_BITBUCKETSERVER_PASSWORD` = The password for the account used to comment.

  This account is then used to provide feedback on your PRs.

  ### Continuous Integration

  Continuous Integration is the process of regularly running tests and generating metrics for a project. It is where 
  you can ensure that the code you are submitting for review is passing on all of the tests. You commonly see this as 
  green or red dots next to commits.

  Danger is built to run as a part of this process, so you will need to have this set up as a pre-requisite.

  ### Setting up Danger to run on your CI

  [jest]: https://facebook.github.io/jest/
  [jest-config]: https://facebook.github.io/jest/docs/configuration.html
  [github_bots]: https://twitter.com/sebastiangrail/status/750844399563608065
  [github_token]: https://github.com/settings/tokens/new
  [yarn]: https://yarnpkg.com
  [dangerfile]: /swift/guides/the_dangerfile.html
  [homebrew]: https://brew.sh
  [arch]: https://danger.systems/swift/tutorials/architecture.html
  [swiftpm_docs]: https://github.com/apple/swift-package-manager/blob/master/Documentation/Usage.md

section.examples
  article
    .split
      .thirty
        ul style="margin-top: 0px;"
          - for source in cis
            - source_id = source["name"].downcase.gsub(" ", "_")
            li class=source_id data-ref="docs_#{source_id}"
              a == source["name"]

      .seventy style="padding:0 20px;"
        - for source in cis
          - source_id = "docs_" + source["name"].downcase.gsub(" ", "_")

          div class=source_id style="display:none;"
            /== markdown_h((source["comment"]["shortText"].strip || "") + source["md"])
            == markdown_h(source["md"])


markdown:
  ### Verify Installation

  You should be able to verify that you have successfully integrated Danger by either re-building your CI or pushing your new commits.

  ## What now?

  There are a few of places you can go from here. We'd recommending opening tabs on all these articles:

  -   [The Dangerfile][dangerfile].
  -   [Cultural Changes of Danger][culture].
  -   [The Dangerfile API reference][api].

  Then depending on the type of project you are working on, checking out [Danger + iOS App][app]. These should give you 
  good overview of what is possible from here, then it's really up to you to find how you can codify some aspects of 
  your team's culture.

  Good luck!

  [api]: /swift/reference.html
  [dangerfile]: /swift/guides/the_dangerfile.html
  [culture]: /swift/usage/culture.html
  [app]: /swift/tutorials/ios_app.html


== javascript_include_tag  "https://code.jquery.com/jquery-3.1.0.min.js"

javascript:
  /// Generic-ish Example Highlight Code
  $("section.examples ul li").mouseover(function(event) {
    event.preventDefault()
    
    // Set the corresponding code
    var klass = $(this).attr("class").replace(" highlighted", "")

    // Set selection
    $(this).parent().children().removeClass("highlighted")
    $(this).addClass("highlighted")

    var selector = $(this).data("ref")
    $(".seventy div").css("display", "none")

    var md = $("." + selector)
    md.css("display", "block")
  })

  $("section.examples ul li:first-child").trigger('mouseenter')
